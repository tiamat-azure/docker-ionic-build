FROM anthonymonori/android-ci-image:latest
RUN wget https://services.gradle.org/distributions/gradle-4.0.2-bin.zip -P /tmp
RUN unzip -d /opt/gradle /tmp/gradle-*.zip
ENV PATH=/opt/gradle/gradle-4.0.2/bin:$PATH
RUN npm install -g ionic cordova yarn

